Fist time setup
```
mkdir YOURTOPDIR
cd YOURTOPDIR
setupATLAS
lsetup git
rcsetup Base,2.4.28
mkdir run
git clone ssh://git@gitlab.cern.ch:7999/palacino/ZprimeAnalysis2021.git ZprimeAnalysis
rc build
```

On every login
```
cd YOURTOPDIR	
setupATLAS
lsetup git
rcsetup Base,2.4.28
```

After making modifications to the code
```
cd YOURTOPDIR
rc build
```
or
```
rc compile_pkg ZprimeAnalysis
```

To run the analysis code
```
cd YOURTOPDIR/run
RunAnalysis OUTPUTDIR
```
OUTPUTDIR has to be unique so that you don't write over existing data.

If the RunAnalysis command doesn't appear, type
```
rc build
```
from YOURTOPDIR


To run the plotting macro HistPlotter.C you need to copy all the output data over to the Plotting directory
```
cd YOURTOPDIR/run
RunAnalysis OUTPUTDIR
cd ../ZprimeAnalysis/Plotting
cp ../../run/OUTPUTDIR_*/hist-*.root .
```
Modify the histogram name string and all the other configurables, then run
```
root -l -q -b HistPlotter.C
```
a pdf file named HISTOGRAMNAME.pdf or HISTOGRAMNAME_log.pdf should appear now in YOURTOPDIR/ZprimeAnalysis/Plotting.